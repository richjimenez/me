module.exports = (doc, name) => {
  // photo
  doc.image('yo.jpg', 440, 292, {
    fit: [354, 500],
    align: 'left',
    valign: 'bottom',
  });

  // binary
  doc
    .font('fonts/HelveticaNowText-Thin.ttf')
    .fillColor('#B2B2B2')
    .fontSize(10)
    .text(
      '01110100 01101000 01101001 01101110 01101011 00100000 01100100 01101001 01100110 01100110 01100101 01110010 01100101 01101110 01110100 00100000 01100001 01101110 01100100 00100000 01100011 01110010 01100101 01100001 01110100 01100101',
      doc.page.width - 220,
      20,
      {
        width: 200,
        lineGap: -2,
        align: 'right',
      },
    );

  doc.fillColor('#000');

  // introduction
  doc
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .fontSize(50)
    .text('HI,', 20, 5)
    .fillColor('#252525')
    .moveDown(-0.35);

  if (name) {
    doc
      .font('fonts/HelveticaNowText-ExtraBold.ttf')
      .text(name)
      .fillColor('#464646')
      .moveDown(-0.4);
  }

  doc
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .text("I'M")
    .fillColor('#636363')
    .moveDown(-0.4);
  doc
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .text('RICK')
    .fillColor('#7d7d7d')
    .moveDown(-0.4);
  doc
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .text('JIMENEZ')
    .moveDown(-0.2);
  doc
    .font('fonts/HelveticaNowText-Thin.ttf')
    .fontSize(25)
    .text('Software engineer')
    .moveDown(0.2);

  // about me
  doc.font('fonts/HelveticaNowText-Light.ttf');
  doc.fontSize(9);
  const description = [
    {
      text: `I have 15 years of work experience in the digital environment, and I have worked primarily in advertising agencies and software development companies. Additionally, I’ve been part of projects including video editing and post production, 3D product modeling, graphic design and photography.`,
      options: {
        lineGap: -2,
        align: 'justify',
      },
    },
    {
      text: `I started working on visual interfaces since flash was the trend in web design, when all the sites contains a rich multimedia interfaces. Since then i've in constant involve with web technologies and their evolution, right now  my work ecosystem (javascript) is react (front end) and firebase / express (back end), but i've worked with several frameworks and technologies in the past.`,
      options: {
        width: 470,
        lineGap: -2,
        align: 'justify',
      },
    },
    {
      text: `I dedicate myself to creating connections between people by merging technology with knowledge.`,
      options: {
        lineGap: -2,
        width: 460,
        align: 'justify',
      },
    },
    {
      text: `I believe in the transformation of all things and manage my philosophy through this world. In a few words, I define myself as a person in constant evolution.`,
      options: {
        width: 450,
        lineGap: -2,
        align: 'justify',
      },
    },
  ];

  description.forEach(p => doc.text(p.text, p.options).moveDown());

  doc.fontSize(35);
  doc
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .text('EMPLOYMENT HISTORY')
    .moveDown(-0.2);

  doc
    .font('fonts/HelveticaNowText-Thin.ttf')
    .fontSize(16)
    .text('(Last 10 Years)')
    .moveDown(0.2);

  const jobs = [
    {
      title: 'Software engineer at Meid Taller, Guadalajara',
      fromTo: 'January 2009 — January 2011',
      options: {
        width: 420,
        lineGap: -2,
        align: 'justify',
      },
      description:
        'I was the leader of the web development division, developing visual and multimedia impact sites with Flash Studio primarily. I also worked on video editing and post production projects',
    },
    {
      title: 'Software engineer at Via Alterna, Guadalajara',
      fromTo: 'January 2012 — January 2014',
      options: {
        width: 420,
        lineGap: -2,
        align: 'justify',
      },
      description: 'I was the development team lead, we create several web based apps with a PHP frameworks (cakephp & mysql) in my time here.',
    },
    {
      title: 'Software engineer at WOM Group, Guadalajara',
      fromTo: 'January 2014 — June 2016',
      options: {
        width: 420,
        lineGap: -2,
        align: 'justify',
      },
      description:
        'I worked here in a several web apps for a large real estate agency in the city creating a CMS for its different urban developments',
    },
    {
      title: 'Development Team Lead at Ryson Studios, Mcallen',
      fromTo: 'July 2016 — October 2019',
      options: {
        width: 430,
        lineGap: -2,
        align: 'justify',
      },
      description:
        'I created and monitored several web and mobile applications, working with some javascript technologies, such as react.js meteor.js, angular, react native, among others. I also worked creating database architectures and analysis of them.',
    },
  ];

  jobs.forEach(job => {
    doc
      .fontSize(10)
      .font('fonts/HelveticaNowText-ExtraBold.ttf')
      .text(job.title)
      .moveDown(0);
    doc
      .font('fonts/HelveticaNowText-Light.ttf')
      .fontSize(8)
      .text(job.fromTo)
      .moveDown(0.3);
    doc
      .fontSize(9)
      .text(job.description, job.options)
      .moveDown(0.5);
  });

  // next page
  doc.addPage();

  doc.fontSize(35);
  doc
    .fillColor('#7d7d7d')
    .font('fonts/HelveticaNowText-ExtraBold.ttf')
    .text('SKILLS')
    .moveDown(-0.2);

  const skills = [
    {
      title: 'JAVASCRIPT',
      percent: '97%',
    },
    {
      title: 'TYPESCRIPT',
      percent: '60%',
    },
    {
      title: 'REACT',
      percent: '92%',
    },
    {
      title: 'REACT NATIVE',
      percent: '88%',
    },
    {
      title: 'REDUX',
      percent: '90%',
    },
    {
      title: 'NESTJS',
      percent: '50%',
    },
    {
      title: 'ANGULAR',
      percent: '60%',
    },
    {
      title: 'MONGODB',
      percent: '90%',
    },
    {
      title: 'MYSQL',
      percent: '75%',
    },
    {
      title: 'METEOR',
      percent: '95%',
    },
    {
      title: 'FIREBASE',
      percent: '85%',
    },
    {
      title: 'PHP',
      percent: '80%',
    },
  ];

  doc.fontSize(40);
  let y = 50;
  skills.forEach(skill => {
    doc
      .fillColor('#979797')
      .font('fonts/HelveticaNowText-ExtraBold.ttf')
      .text(skill.title, 20, y)
      .fillColor('#bdbdbd')
      .text(skill.percent, doc.widthOfString(skill.title) + 30, y);
    y += 35;
  });
};
