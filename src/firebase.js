import * as firebase from 'firebase/app';
import 'firebase/functions';

// TODO add this value to env vars
const config = {
  apiKey: 'AIzaSyAWS_znbjm8JDCjYUlJB6HOw09tYvIChQ8',
  authDomain: 'rick-d3a8c.firebaseapp.com',
  databaseURL: 'https://rick-d3a8c.firebaseio.com',
  projectId: 'rick-d3a8c',
  storageBucket: 'rick-d3a8c.appspot.com',
  messagingSenderId: '67391814005',
  appId: '1:67391814005:web:2a3d256aa3f93ea1c67b3c',
  measurementId: 'G-361YGD3RWQ',
};
const app = firebase.initializeApp(config);

if (process.env.NODE_ENV === 'development')
  app.functions().useFunctionsEmulator('http://localhost:5001');

var functions = app.functions();

export { functions };
