import React from 'react';
import { useTrail, animated } from 'react-spring';

export default () => {
  const binary =
    '01110100 01101000 01101001 01101110 01101011 00100000 01100100 01101001 01100110 01100110 01100101 01110010 01100101 01101110 01110100 00100000 01100001 01101110 01100100 00100000 01100011 01110010 01100101 01100001 01110100 01100101';
  const quote = binary.split('').map(e => (e === '1' || e === '0' ? e : '-'));
  const config = { mass: 1, tension: 2000, friction: 25 };
  const trail = useTrail(quote.length, {
    from: { opacity: 0 },
    to: { opacity: 1 },
    delay: 1000,
    config,
  });

  return (
    <div>
      {trail.map((props, index) => {
        return (
          <animated.div key={`digit-${index}`} className="digit" style={props}>
            {quote[index]}
          </animated.div>
        );
      })}
    </div>
  );
};
