import React, { useState } from 'react';
import axios from 'axios';
import BinaryQuote from './components/BinaryQuote';

import './App.scss';

function App() {
  const [name, setName] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const getResume = () => {
    setIsLoading(true);
    let url;
    if (process.env.NODE_ENV === 'development') url = 'http://localhost:5001/rick-d3a8c/us-central1/api/getResume';
    else url = 'https://us-central1-rick-d3a8c.cloudfunctions.net/api/getResume';

    axios({
      url,
      method: 'GET',
      responseType: 'blob',
      params: { name },
    }).then((response) => {
      setIsLoading(false);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'rick-resume.pdf');
      document.body.appendChild(link);
      link.click();
    });
  };

  return (
    <div className="app">
      <div className="center">
        <h1>
          RICK <span>JIMENEZ</span>
        </h1>
        <h2>Software engineer</h2>

        <span>Hi,</span>
        <input placeholder="Your Name" maxLength="8" onChange={(e) => setName(e.target.value)} value={name} />
        <span>I'm Baking some interesting code stuff, but you can</span>
        <button onClick={getResume} disabled={isLoading}>
          {isLoading ? 'Generating my resume for you...' : 'Meanwhile download my resume'}
        </button>
        <span> also you can check my </span>
        <button onClick={() => window.open('https://gitlab.com/richjimenez')}>GitLab profile</button>
        <br />
        <BinaryQuote />
      </div>
    </div>
  );
}

export default App;
